#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 11:39:58 2020

@author: evan
"""

import sys
from argparse import Namespace
import pytest
from pathlib import Path
from print_partial_datasets import print_partial_datasets, setup_parser, main
from unittest.mock import patch


@pytest.fixture(scope="module")
def datadir():
    return Path("tests/testdata/")


@pytest.fixture(scope="module")
def treepath():
    return Path("file_tree/biomox.tree")


def test_out_txt(capsys, datadir, treepath):
    """Test against sample dataset"""
    knowntext = (
        Path("tests/testdata/test_print_table.txt").open("r").read()
    )
    # Capture stdout
    print_partial_datasets(
        datadir,
        treepath,
        ["raw_T1", "raw_bold", "raw_fmap_mag", "raw_fmap_ph"],
        ["participant"],
        session=["01", "02"],
    )
    captured = capsys.readouterr()

    assert captured.out == knowntext


@patch("print_partial_datasets.print_partial_datasets")
def test_main(mock_ppd):
    with patch.object(
        sys,
        "argv",
        "./print_filetree_table.py -d /path/to/dir -f file_tree/biomox.tree -s raw_T1 raw_bold -v participant session".split(),
    ):
        main()
        mock_ppd.assert_called_once_with(
            "/path/to/dir",
            "file_tree/biomox.tree",
            ["raw_T1", "raw_bold"],
            ["participant", "session"],
        )


@pytest.mark.parametrize(
    "cmd_string,expected",
    [
        (
            "-d path/to/dir -f file_tree/biomox.tree -s raw_T1 raw_bold raw_fmap_mag raw_fmap_ph -v participant session",
            Namespace(
                datadir=["path/to/dir"],
                filetree=["file_tree/biomox.tree"],
                short_name=["raw_T1", "raw_bold", "raw_fmap_mag", "raw_fmap_ph"],
                variables=["participant", "session"],
            ),
        )
    ],
)
def test_argument_parser(datadir, treepath, cmd_string, expected):
    assert setup_parser().parse_args(cmd_string.split()) == expected


def test_noargs():
    with pytest.raises(SystemExit):
        with patch.object(sys, "argv", ["./print_filetree_table.py"]):
            main()
